package com.twojeaukcje.portalAukcyjny;

import com.twojeaukcje.portalAukcyjny.entity.Auction;
import com.twojeaukcje.portalAukcyjny.repository.AuctionRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import java.util.Date;

@EnableJpaRepositories
@SpringBootApplication
public class PortalAukcyjnyApplication {

    public static void main(String[] args) {
		ConfigurableApplicationContext context = SpringApplication.run(PortalAukcyjnyApplication.class, args);
		AuctionRepository auctionRepository = context.getBean(AuctionRepository.class);
		auctionRepository.save(new Auction(null,null,"Edek","dsa", new Date(),new Date(),1,2));
		auctionRepository.save(new Auction(null,null,"Edek","dsa", new Date(),new Date(),1,2));
		auctionRepository.save(new Auction(null,null,"Edek","dsa", new Date(),new Date(),1,2));
		auctionRepository.save(new Auction(null,null,"Edek","dsa", new Date(),new Date(),1,2));
	}

}
