package com.twojeaukcje.portalAukcyjny.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "auction_observers")
public class AuctionObserv {
    @Id
    private Long id;
    @OneToOne
    private Auction auction;
    @OneToOne
    private User user;
}
