package com.twojeaukcje.portalAukcyjny.entity;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "auctions")
public class Auction {
    @Id
    @GeneratedValue
    private Long id;
    @ManyToOne
    private User user;
    private String name;
    private String category;
    private Date startDate;
    private Date endDate;
    private int startingPrice;
    private int buyNow;

    public Auction() {
    }

    public Auction(Long id, User user, String name, String category, Date startDate, Date endDate, int startingPrice, int buyNow) {
        this.id = id;
        this.user = user;
        this.name = name;
        this.category = category;
        this.startDate = startDate;
        this.endDate = endDate;
        this.startingPrice = startingPrice;
        this.buyNow = buyNow;
    }

    @OneToMany
    private List<AuctionBid> bids;
    @OneToMany
    private List<AuctionPhotos> photos;


    public Long getId() {
        return id;
    }

    public Auction setId(Long idAuction) {
        this.id = idAuction;
        return this;
    }

    public User getUser() {
        return user;
    }

    public Auction setUserId(User user) {
        this.user = user;
        return this;
    }

    public String getName() {
        return name;
    }

    public Auction setName(String name) {
        this.name = name;
        return this;
    }

    public String getCategory() {
        return category;
    }

    public Auction setCategory(String category) {
        this.category = category;
        return this;
    }

    public Date getStartDate() {
        return startDate;
    }

    public Auction setStartDate(Date startDate) {
        this.startDate = startDate;
        return this;
    }

    public Date getEndDate() {
        return endDate;
    }

    public Auction setEndDate(Date endDate) {
        this.endDate = endDate;
        return this;
    }

    public int getStartingPrice() {
        return startingPrice;
    }

    public Auction setStartingPrice(int startingPrice) {
        this.startingPrice = startingPrice;
        return this;
    }

    public int getBuyNow() {
        return buyNow;
    }

    public Auction setBuyNow(int buyNow) {
        this.buyNow = buyNow;
        return this;
    }
}