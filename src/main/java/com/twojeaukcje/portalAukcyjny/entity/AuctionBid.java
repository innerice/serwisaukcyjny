package com.twojeaukcje.portalAukcyjny.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "auction_bids")
public class AuctionBid {

    @Id
    private Long id;
    @ManyToOne
    private User user;
    @ManyToOne
    private Auction auction;
    private Date createdAt;
    private int price;

    public AuctionBid(Long id, User user, Auction auction, Date createdAt, int price) {
        this.id = id;
        this.user = user;
        this.auction = auction;
        this.createdAt = createdAt;
        this.price = price;
    }
}
