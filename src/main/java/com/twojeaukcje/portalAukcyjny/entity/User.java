package com.twojeaukcje.portalAukcyjny.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.Date;
import java.util.List;
@Entity
public class User {
    @Id
    private Long id;
    private String username;
    private String password;
    private String email;
    private Date lastLog;
    private Date regiserDate;

    public User(Long id, String login, String password, String email, Date lastLog, Date regiserDate) {
        this.id = id;
        this.username = login;
        this.password = password;
        this.email = email;
        this.lastLog = lastLog;
        this.regiserDate = regiserDate;
    }

    @OneToMany
    private List<Auction> auctions;



    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                ", lastLog=" + lastLog +
                ", regiserDate=" + regiserDate +
                '}';
    }

    public Long getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getEmail() {
        return email;
    }
}
