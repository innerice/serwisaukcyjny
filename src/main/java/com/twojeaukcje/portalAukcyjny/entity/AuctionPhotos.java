package com.twojeaukcje.portalAukcyjny.entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "auction_photos")
public class AuctionPhotos {

    @Id
    private Long id;
    @ManyToOne
    private Auction auction;
    @OneToOne
    private User user;
    private String trackToPhoto;
    private Date addDate;

    public AuctionPhotos(Long id, Auction auction, User user, String trackToPhoto, Date addDate) {
        this.id = id;
        this.auction = auction;
        this.user = user;
        this.trackToPhoto = trackToPhoto;
        this.addDate = addDate;
    }

    @Override
    public String toString() {
        return "AuctionPhotos{" +
                "id=" + id +
                ", auction=" + auction +
                ", user=" + user +
                ", trackToPhoto='" + trackToPhoto + '\'' +
                ", addDate=" + addDate +
                '}';
    }

    public Long getId() {
        return id;
    }

    public AuctionPhotos setId(Long id) {
        this.id = id;
        return this;
    }

    public Auction getAuction() {
        return auction;
    }

    public AuctionPhotos setAuction(Auction auction) {
        this.auction = auction;
        return this;
    }

    public User getUser() {
        return user;
    }

    public AuctionPhotos setUser(User user) {
        this.user = user;
        return this;
    }

    public String getTrackToPhoto() {
        return trackToPhoto;
    }

    public AuctionPhotos setTrackToPhoto(String trackToPhoto) {
        this.trackToPhoto = trackToPhoto;
        return this;
    }

    public Date getAddDate() {
        return addDate;
    }

    public AuctionPhotos setAddDate(Date addDate) {
        this.addDate = addDate;
        return this;
    }
}
