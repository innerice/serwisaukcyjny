package com.twojeaukcje.portalAukcyjny.controller;

import com.twojeaukcje.portalAukcyjny.entity.Auction;
import com.twojeaukcje.portalAukcyjny.repository.AuctionRepository;
import com.twojeaukcje.portalAukcyjny.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Optional;

@Controller
@RequestMapping("/")
public class HelloController {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private AuctionRepository auctionRepository;

    @GetMapping("/hello")
    public String index(Model model) {
        Iterable<Auction> auctions = auctionRepository.findAll();
        model.addAttribute("auctions",auctions);

        return "hello";


    }
}
