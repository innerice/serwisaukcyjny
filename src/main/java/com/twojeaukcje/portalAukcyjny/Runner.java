package com.twojeaukcje.portalAukcyjny;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Runner {
    @RequestMapping("/")
    public String index() {
        return "Portal aukcyjny";
    }
}
