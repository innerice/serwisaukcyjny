package com.twojeaukcje.portalAukcyjny.repository;

import com.twojeaukcje.portalAukcyjny.entity.Auction;
import org.springframework.data.repository.CrudRepository;

public interface AuctionRepository extends CrudRepository  <Auction, Long >  {

}
