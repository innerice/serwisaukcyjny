package com.twojeaukcje.portalAukcyjny.repository;

import com.twojeaukcje.portalAukcyjny.entity.User;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long> {
}
